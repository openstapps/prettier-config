# @openstapps/prettier-config

[![pipeline status](https://img.shields.io/gitlab/pipeline/openstapps/prettier-config.svg?style=flat-square)](https://gitlab.com/openstapps/prettier-config/commits/master)
[![npm](https://img.shields.io/npm/v/@openstapps/prettier-config.svg?style=flat-square)](https://npmjs.com/package/@openstapps/prettier-config)
[![license)](https://img.shields.io/npm/l/@openstapps/prettier-config.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Installation

```shell
npm install -D prettier
npm install -D @openstapps/prettier-config
```

Add the following to your `package.json`
```json
{
  "prettier": "@openstapps/prettier-config"
}
```

Also make sure you are following the installation guidelines
for [eslint-config-prettier](https://github.com/prettier/eslint-config-prettier#eslint-config-prettier)
so the two won't interfere.

After that, follow the Prettier guides for
[IDE Integration](https://prettier.io/docs/en/editors.html)
and take a look at [CLI Commands](https://prettier.io/docs/en/cli.html)
for pipelines.
